////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol ComputerPart {
    func accept(computerPartVisitor:ComputerPartVisitor)
}

class Keyboard:ComputerPart {
    
    func accept(computerPartVisitor:ComputerPartVisitor ) {
        computerPartVisitor.visit(keyboard: self);
    }
}

class Monitor : ComputerPart {
    
    func accept(computerPartVisitor:ComputerPartVisitor ) {
        computerPartVisitor.visit(monitor: self);
    }
}

class Mouse : ComputerPart {
    
    func accept(computerPartVisitor:ComputerPartVisitor ) {
        computerPartVisitor.visit(mouse: self);
    }
}

class Computer : ComputerPart {
    
    var parts:[ComputerPart]?

    init(){
        parts = [ Mouse(), Keyboard(), Monitor()]
    }
    
    func accept(computerPartVisitor:ComputerPartVisitor ) {
        
        for part in parts!{
            part.accept(computerPartVisitor: computerPartVisitor)
        }
        computerPartVisitor.visit(computer: self)
    }
}

protocol ComputerPartVisitor {
    func visit(computer:Computer )
    func visit(mouse:Mouse )
    func visit(keyboard:Keyboard )
    func visit(monitor:Monitor )
}

class ComputerPartDisplayVisitor : ComputerPartVisitor {
    
    func visit(computer:Computer ) {
        print("Displaying Computer.")
    }
    
    func visit(mouse:Mouse ) {
        print("Displaying Mouse.")
    }
    
    func visit(keyboard:Keyboard ) {
        print("Displaying Keyboard.")
    }
    
    func visit(monitor:Monitor) {
        print("Displaying Keyboard.")
    }
}
