////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

class Observer {
    var subject: Subject?
    func update(){}
}

class Subject {
    
    private var observers :Array<Observer> = Array<Observer>()
    
    var state:Int = 0{
        didSet{ notifyAllObservers() }
    }
    
    func attach(observer:Observer){
        observers.append(observer)
    }
    
    func notifyAllObservers(){
        for observer in observers{
            observer.update()
        }
    }
}

class BinaryObserver : Observer{
    
    init(subject:Subject ){
        super.init()
        self.subject = subject
        self.subject!.attach(observer: self)
    }
    
    override func update() {
        print("Binary String:\(String( self.subject!.state , radix: 2))")
    }
}

class OctalObserver : Observer{
    
    init(subject:Subject ){
        super.init()
        self.subject = subject
        self.subject!.attach(observer: self)
    }
    
    override func update() {
        print("Octal String:\(String( self.subject!.state , radix: 8))")
    }
}

class HexaObserver : Observer{
    
    init(subject:Subject ){
        super.init()
        self.subject = subject
        self.subject!.attach(observer: self)
    }
    
    override func update() {
        print("Hex String:\(String( self.subject!.state , radix: 16))")
    }
}
