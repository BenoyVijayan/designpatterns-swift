////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation


protocol MediaPlayer {
    func play(audioType:String, fileName:String)
}

protocol AdvancedMediaPlayer {
    func playVlc(fileName:String)
    func playMp4(fileName:String)
}

class VlcPlayer:AdvancedMediaPlayer{
    
    func playVlc(fileName:String){
        print("Playing VLC")
    }
    
    func playMp4(fileName:String){}
}

class Mp4Player:AdvancedMediaPlayer{

    func playMp4(fileName:String){
        print("Playing Mp4")
    }
    
    func playVlc(fileName:String){}
}

class MediaAdapter:MediaPlayer{

    var advancedMediaPlayer:AdvancedMediaPlayer?
    init(audioType:String){
        if audioType == "vlc"{
            advancedMediaPlayer = VlcPlayer()
        }else if audioType == "mp4"{
            advancedMediaPlayer = Mp4Player()
        }
    }
    
    func play(audioType:String , fileName:String){
        if audioType == "vlc"{
            advancedMediaPlayer?.playVlc(fileName: fileName)
        }else if audioType == "mp4"{
            advancedMediaPlayer?.playMp4(fileName: fileName)
        }
    }
}

class AudioPlayer:MediaPlayer{
    func play(audioType:String,fileName:String){
        if audioType == "mp3"{
            print("Playing mp3")
        }else{
            let adapter:MediaAdapter = MediaAdapter(audioType:audioType)
            adapter.play(audioType:audioType,fileName:fileName)
        }
    }
}
