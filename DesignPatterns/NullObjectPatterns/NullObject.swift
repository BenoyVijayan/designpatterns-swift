////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation
protocol AbstractCustomer {
    var name:String{get set}
    var isNil:Bool{ get }
}

class RealCustomer:AbstractCustomer {
    
    init(name:String){  self.name = name }
    var name:String
    var isNil:Bool{ get{ return false } }
}

class NullCustomer : AbstractCustomer {
    
    var name:String = "Not available in customer database"
    var isNil:Bool{ get{ return true } }
}

class CustomerFactory {
    
    static let names:[String]  = ["Rob", "Joe", "Julie"]
    static func getCustomer(name:String )->AbstractCustomer{
        for nm in  CustomerFactory.names{
            if name.uppercased() == nm.uppercased(){return RealCustomer(name: name) }
        }
        
        return NullCustomer()
    }
}
