////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol Car {
    func drive()
}

class SuvCar:Car,Equatable{
    
    var color:String = ""
    var fuel:String = ""
    var seatingCapacity:Int = 0
    
    init(color:String){
        
        self.color  = color;
        print("SUV Car Created")
    }
    
    func drive() {
        print("Drive SUV with properties - Color : \(color) , Fuel : \(fuel) , Seating Capacity:\(seatingCapacity)")
    }
    
    static func ==(lhs: SuvCar, rhs: SuvCar) -> Bool {
        return lhs.color == rhs.color
    }
}

class CarFactory{
    
    private static var carDictionary:Dictionary<String,Car> = Dictionary<String,Car>()
    
    static func getSuvCar(color:String)->Car{
        
        guard let suvCar = carDictionary[color] else{
            let newCar = SuvCar(color: color)
            carDictionary[color] = newCar
            
            return newCar
        }
        
        return suvCar
    }
}
