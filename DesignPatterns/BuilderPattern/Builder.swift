////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol View {
    
    var name:String{ get }
    var title:Title?{ get }
    var tag:Int{ get }
    init()
}

protocol Title {
    func fillColor()
}

class ImageTitle: Title{
    
    func fillColor() {
        print("Fill Background Color")
    }
}

class StringTitle: Title{
    
    func fillColor() {
        print("Fill Text Color")
    }
}

class Button : View{
    
    var title:Title?{
        get{ return ImageTitle() }
    }
    
    var name: String{
        
        get{ return "Button" }
    }
    
    var tag: Int{
        
        get{
            return self.tag
        }
    }

    required init(){}
}

class Label: View{
    
    var title: Title?{
        get{
            return StringTitle()
        }
    }
    
    var name: String{
        
        get{
            return "Label"
        }
    }
    
    var tag: Int{
        
        get{
            return self.tag
        }
    }
    
    required init(){}
}

class Component{
    
    private var views:Array<View> = Array<View>()
    
    func addView(view:View){
        views.append(view)
    }
    
    func showViews(){
        
        for view in views{
            print("View Name \(view.name)")
            view.title?.fillColor()
            print("View Tag \(view.tag)")
        }
    }
}

class ComponentBuilder{
    
    func createRoundComponentes()->Component{
        
        let component = Component()
        
        let roundButton = RoundButton()
        component.addView(view: roundButton)
        
        let roundCorneredLabel = RoundCorneredLabel()
        component.addView(view: roundCorneredLabel)
        
        return component
    }
    
    func createRectangleComponentes()->Component{
        
        let component = Component()
        
        let squareButton = SquareButton()
        component.addView(view: squareButton)
        
        let rectangleLabel = RectangleLabel()
        component.addView(view: rectangleLabel)
        
        return component
    }
}

class RoundButton:Button{
    
    override var name: String{
        return "Round Button"
    }
    
    override var tag: Int{
        return 1
    }
}

class SquareButton:Button{
    
    override var name: String{
        return "Square Button"
    }
    
    override var tag: Int{
        return 2
    }
}

class RectangleLabel:Label{
    
    override var name: String{
        return "Rectangle Label"
    }
    
    override var tag: Int{
        return 3
    }
}

class RoundCorneredLabel:Label{
    
    override var name: String{
        return "Round cornered Label"
    }

    override var tag: Int{
        return 4
    }
}


