////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol Iterator {
    var hasNext:Bool{ get }
    var next:Any?{ get }
}

protocol Container {
    var iterator:Iterator? { get  set }
}

class NameRepository:Container{
    
    let names:Array = ["Robert" , "John" ,"Julie" , "Lora"]
    var iterator:Iterator? = nil
    
    init(){ iterator = NameIterator(master: self) }
   
    class NameIterator:Iterator{
        
        var index:Int = 0
        var master:NameRepository?
        var hasNext: Bool{
            get{
                if index < master!.names.count{ return true }
                return false
            }
        }
        
        var next:Any?{
            get{
                if self.hasNext == true{
                    let retVal = master?.names[index]
                    index += 1
                    return retVal
                }
                return nil
            }
        }
        
        init(master:NameRepository){ self.master = master }
    }
}
