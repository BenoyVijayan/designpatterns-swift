////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
import Foundation

class AbstractLogger{
    
    public static var  info:Int = 1
    public static var debug:Int = 2
    public static var error:Int = 3
    public var level:Int?
    public var nextLogger:AbstractLogger?
   
    public func logMessage( level:Int,message:String){
        if level <= level{
            write(message: message)
        }
        if nextLogger != nil{
            nextLogger?.logMessage(level: level, message: message)
        }
    }
        
    public func write(message:String){}
}

class ConsoleLogger:AbstractLogger{
    
    init( level:Int){
        super.init()
        self.level = level
    }
    
    override func write(message: String) {
        print("Standard Console::Logger: \(message)" )
    }
}

class ErrorLogger:AbstractLogger{
    init( level:Int){
        super.init()
        self.level = level
    }
    
    override func write(message: String) {
        print("Error Console::Logger: \(message)" )
    }
}

class FileLogger:AbstractLogger{
    
    init( level:Int){
        super.init()
        self.level = level
    }
    
    override func write(message: String) {
        print("File Logger: \(message)" )
    }
}


