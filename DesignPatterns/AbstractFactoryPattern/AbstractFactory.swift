////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

enum FactoryType{
    
    case shape
    case color
}

protocol AbstractFactory{
    
    func getShape(shapeType:ShapeType)->Shape?
    func getColor(colorType:ColorType)->Color?
}

extension ShapeFactory : AbstractFactory{
    
    func getShape(shapeType:ShapeType)->Shape?{
        
        let shape : Shape
        switch shapeType{
            case .rectangle:
                shape = Rectangle()
            
            case .triangle:
                shape = Triangle()
        }
        return shape
    }
    
    func getColor(colorType : ColorType) -> Color? {
        return nil
    }
}

extension ColorFactory : AbstractFactory{
    
    func getColor(colorType:ColorType)->Color?{
        
        let color : Color
        switch colorType{
            
            case .red:
                color = Red()
                
            case .blue:
                color = Blue()
        }
        return color
    }
    
    func getShape(shapeType:ShapeType)->Shape?{
        
        return nil
    }
}

class FactoryProducer{
    
    static func getFactory(factoryType:FactoryType)->AbstractFactory{
        
        switch factoryType {
        case .shape:
            return ShapeFactory()
        case .color:
            return ColorFactory()
        }
    }
}


