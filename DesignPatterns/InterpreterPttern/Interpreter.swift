////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol Expression {
    func interpret(context:String)->Bool
}

class TerminalExpression:Expression {
    
    private var data:String = ""
    
    init(data:String ){
        self.data = data
    }
    
    func interpret(context:String)->Bool {

        if context.contains(data) {
            return true
        }
        
        return false
    }
}

class OrExpression:Expression {
    
    private var  expr1:Expression? = nil
    private var expr2:Expression? = nil
    
    init(expr1:Expression , expr2:Expression ) {
        self.expr1 = expr1
        self.expr2 = expr2
    }
    
    func interpret(context:String )->Bool {
        return (expr1!.interpret(context: context) || expr2!.interpret(context: context))
    }
}

class AndExpression : Expression {
    
    private var expr1:Expression? = nil
    private var expr2:Expression? = nil
    
    init(expr1:Expression , expr2:Expression ) {
        self.expr1 = expr1
        self.expr2 = expr2
    }
    
    func interpret(context:String )->Bool {
        return (expr1!.interpret(context: context) && expr2!.interpret(context: context))
    }
}
