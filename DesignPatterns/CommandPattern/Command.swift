////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

protocol Order{
    func execute()
}

class Stock{
    
    var name:String = "XYZ"
    var quatity:Int = 5
    
    func buy(){
        print("Bought \(quatity) '\(name)' ")
    }
    
    func sell(){
        print("Sold \(quatity) '\(name)' ")
    }
}

class BuyStock:Order{

    private var xyzStock:Stock?
    
    init( _ xyzStock:Stock){
        self.xyzStock = xyzStock
    }
    
    func execute() {
        xyzStock!.buy()
    }
}

class SellStock:Order{
    private var xyzStock:Stock?
    
    init( _ xyzStock:Stock){
        self.xyzStock = xyzStock
    }
    
    func execute() {
        xyzStock!.sell()
    }
}

public class Broker {
    
    private var orderList:Array<Order> =  Array<Order>()
    
    func takeOrder(order:Order ){
        orderList.append(order);
    }
    
    public func placeOrders(){
    
        for order in orderList{
            order.execute()
        }
        orderList.removeAll()
    }
}
