////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2017, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation


/* Creational Pattern */

/* Factory Pattern */

print("Factory Pattern\n***************************************")

var rectangle = ShapeFactory.getShape(shapeType: .rectangle)
rectangle.draw()

var triangle = ShapeFactory.getShape(shapeType: .triangle)
triangle.draw()

let red = ColorFactory.getColor(colorType: .red)
red.fill()

let blue = ColorFactory.getColor(colorType: .blue)
blue.fill()


/* Abstract Factory Pattern */

print("\nAbstract Factory Pattern\n***********************************")

let shapeFactory:ShapeFactory = FactoryProducer.getFactory(factoryType: .shape)  as! ShapeFactory

rectangle = shapeFactory.getShape(shapeType: .rectangle)!
rectangle.draw()

triangle = shapeFactory.getShape(shapeType: .triangle)!
triangle.draw()

let colorFactory: ColorFactory = FactoryProducer.getFactory(factoryType: .color) as! ColorFactory

let redColor = colorFactory.getColor(colorType: .red)
redColor?.fill()

let blueColor = ColorFactory.getColor(colorType: .blue)
blueColor.fill()

/* Singleton Pattern */

print("\nSingleton Pattern\n***********************************")


Singleton.singleInstance.first()
Singleton.singleInstance.second()



/* Builder Pattern */

print("\nBuilder Pattern\n***********************************")

let componentBuilder = ComponentBuilder()

let roundComponents = componentBuilder.createRoundComponentes()
let rectangleComponents = componentBuilder.createRectangleComponentes()

roundComponents.showViews()

rectangleComponents.showViews()

/* Prototype Pattern */

print("\nPrototype Pattern\n***********************************")


let elephant = Elephant()
elephant.name = "ELEPHANT1"
elephant.type = "Herbivores"

let elephant1 = elephant.clone()
elephant1.name = "ELEPAHNT2"


print("Elephant - \(elephant.name)")
print("Elephant clone - \(elephant1.name)")

print("Elephant - \(elephant.type)")
print("Elephant clone - \(elephant1.type)")

/* Adapter Pattern */

print("\nAdapter Pattern\n***********************************")

let audioPlayer:AudioPlayer = AudioPlayer()

audioPlayer.play(audioType: "mp3", fileName: "beyond the horizon.mp3")
audioPlayer.play(audioType: "mp4", fileName: "alone.mp4")
audioPlayer.play(audioType: "vlc", fileName: "far far away.vlc")
audioPlayer.play(audioType: "avi", fileName: "mind me.avi")

/* Bridge Pattern */

print("\nBridge Pattern\n***********************************")

let redCircle:Drawable = Circle(x: 5, y: 5, radius: 10, drawApi: RedCircle())
let greenCircle:Drawable = Circle(x: 5, y: 5, radius: 10, drawApi: GreenCircle())
redCircle.draw()
greenCircle.draw()

/* Filter Pattern */

print("\nFilter Pattern\n***********************************")

func printPersons(persons:Array<Person>){
    for person in persons{
        print(person.name)
    }
}

var persons:Array<Person> = Array<Person>()


persons.append(Person(name: "Robert",gender: "Male", maritalStatus: "Single"))
persons.append(Person(name: "John", gender: "Male", maritalStatus: "Married"))
persons.append(Person(name: "Laura", gender: "Female", maritalStatus: "Married"))
persons.append(Person(name: "Diana", gender: "Female", maritalStatus: "Single"))
persons.append(Person(name: "Mike", gender: "Male", maritalStatus: "Single"))
persons.append(Person(name: "Bobby", gender: "Male", maritalStatus: "Single"))

let male:Criteria = CriteriaMale()
let female:Criteria =  CriteriaFemale()
let single:Criteria =  CriteriaSingle()
let singleMale:Criteria = AndCriteria(criteria: single, otherCriteria: male)
let singleOrFemale:Criteria = OrCriteria(criteria: single, otherCriteria: female)

print("Males: ");
printPersons(persons: male.meetCriteria(persons: persons));

print("\nFemales: ");
printPersons(persons: female.meetCriteria(persons: persons));

print("\nSingle Males: ");
printPersons(persons: singleMale.meetCriteria(persons: persons));

print("\nSingle Or Females: ");
printPersons( persons: singleOrFemale.meetCriteria(persons: persons));

/* Composite Pattern */
print("\nComposite Pattern\n***********************************")

let ceo:Employee = Employee(name: "John",department: "CEO", salary: 30000)

let headSales:Employee = Employee(name: "Robert",department: "Head Sales", salary: 20000)

let headMarketing:Employee = Employee(name: "Michel",department: "Head Marketing", salary: 20000)

let clerk1:Employee  =  Employee(name: "Laura",department: "Marketing", salary: 10000)
let clerk2:Employee =  Employee(name: "Bob",department: "Marketing", salary: 10000)

let salesExecutive1:Employee =  Employee(name: "Richard",department: "Sales", salary: 10000)
let salesExecutive2:Employee =  Employee(name: "Rob",department: "Sales", salary: 10000)

ceo.add(employee: headSales)
ceo.add(employee: headMarketing)

headSales.add(employee: salesExecutive1)
headSales.add(employee: salesExecutive2)

headMarketing.add(employee: clerk1)
headMarketing.add(employee: clerk2)

print(ceo)

/* Decorator Pattern */

print("\nDecorator Pattern\n***********************************")


var macbookRegular: MacBook = MacBookRegular()
print("Cost : £\(macbookRegular.cost), Description: \(macbookRegular.description)")

macbookRegular = ProcessorUpgrade(macbook: macbookRegular)
print("Cost : £\(macbookRegular.cost), Description: \(macbookRegular.description)")

macbookRegular = SSDUpgrade(macbook: macbookRegular)
print("Cost : £\(macbookRegular.cost), Description: \(macbookRegular.description)")

macbookRegular = TouchBarUpgrade(macbook: macbookRegular)
print("Cost : £\(macbookRegular.cost), Description: \(macbookRegular.description)\n")

/* Facade Pattern */

print("\nFacade Pattern\n***********************************")

let toyMaker = ToyMaker()

toyMaker.makePlasticToy()
toyMaker.makeWoodenToy()

/* Flyweight Pattern */

print("\nFlyweight Pattern\n***********************************")

let car1:SuvCar = CarFactory.getSuvCar(color: "red") as! SuvCar
let car2:SuvCar =  CarFactory.getSuvCar(color: "blue") as! SuvCar
let car3:SuvCar = CarFactory.getSuvCar(color: "green") as! SuvCar
let car4:SuvCar = CarFactory.getSuvCar(color: "green") as! SuvCar

if car3 == car4 {
    print("car3 and car4 are same objects")
}

if car1 != car2{
    print("car1 and car2 are not  same objects")
}

/* Proxy Pattern */

print("\nProxy Pattern\n***********************************")

let image = ProxyImage(fileName: "test_10mb.jpg");

//image will be loaded from disk
image.display();
print("\n");

//image will not be loaded from disk
image.display()


/* Behavioral Pattern */

/* Chain Of Responsibility Pattern */

print("\n Chain Of Responsibility Pattern\n***********************************")

fileprivate func getChainOfLoggers() -> AbstractLogger{
    
    let errorLogger = ErrorLogger(level: AbstractLogger.error)
    let fileLogger = FileLogger(level: AbstractLogger.debug)
    let consoleLogger = ConsoleLogger(level: AbstractLogger.info)
    
    errorLogger.nextLogger = fileLogger
    fileLogger.nextLogger = consoleLogger
    
    return errorLogger;
}

let loggerChain = getChainOfLoggers()
loggerChain.logMessage(level: AbstractLogger.info,message: "This is an information.")
loggerChain.logMessage(level: AbstractLogger.debug,message: "This is an debug level information.")
loggerChain.logMessage(level: AbstractLogger.error,message: "This is an error information.")

/* Command Pattern */

print("\n Command Pattern\n***********************************")


let xyzStock: Stock = Stock()

let buyStockOrder:BuyStock = BuyStock(xyzStock)

let sellStockOrder:SellStock =  SellStock(xyzStock)

let broker:Broker = Broker()
broker.takeOrder(order: buyStockOrder)
broker.takeOrder(order: sellStockOrder);

broker.placeOrders();

/* Interpreter Pattern */

print("\n Interpreter Pattern\n***********************************")

//Rule: Robert and John are male
func getMaleExpression()->Expression{
    
    let robert:Expression = TerminalExpression(data: "Robert")
    let john:Expression =  TerminalExpression(data: "John")
    return  OrExpression(expr1: robert, expr2: john)
}

//Rule: Julie is a married women
func  getMarriedWomanExpression()->Expression{
    let julie:Expression =  TerminalExpression(data: "Julie")
    let married:Expression =  TerminalExpression(data: "Married")
    return  AndExpression(expr1: julie, expr2: married)
}


let isMale:Expression = getMaleExpression()
let isMarriedWoman:Expression = getMarriedWomanExpression()

print("John is male? \(isMale.interpret(context: "John"))" )
print("Julie is a married women? \(isMarriedWoman.interpret(context: "Married Julie"))" )


/* Iterator Pattern */

print("\n Iterator Pattern\n***********************************")

let namesRepository:NameRepository = NameRepository()

while let name = namesRepository.iterator?.next{
    print(name)
}

/* Mediator Pattern */

print("\n Mediator Pattern\n***********************************")

let robert:User =  User(name: "Robert")
let john:User = User(name: "John")

robert.sendMessage(message: "Hi! John!")
john.sendMessage(message: "Hello! Robert!")

/* Memento Pattern */

print("\n Memento Pattern\n***********************************")


let originator:Originator = Originator()
let careTaker:CareTaker =  CareTaker()

originator.state = "State #1"
originator.state = "State #2"
careTaker.add(state: originator.saveStateToMemento());

originator.state = "State #3";
careTaker.add(state: originator.saveStateToMemento());

originator.state = "State #4"
print("Current State: \( originator.state)")


originator.getStateFromMemento(memento: careTaker.get(index: 0));
print("First saved State: \( originator.state)")

originator.getStateFromMemento(memento: careTaker.get(index: 1));
print("Second saved State: \( originator.state)")

/* Observer Pattern */

print("\n Observer Pattern\n***********************************")

let subject:Subject =  Subject()

_ = HexaObserver(subject: subject)
_ = OctalObserver(subject: subject)
_ = BinaryObserver(subject: subject)

print("First state change: 15")
subject.state = 15

print("Second state change: 10")
subject.state = 10

/* State Pattern */

print("\n State Pattern\n***********************************")

let context:Context =  Context()

let startState:StartState = StartState()
startState.doAction(context: context)

print(context.state!.string )

let stopState:StopState =  StopState()
stopState.doAction(context: context)

print(context.state!.string )


/* Null Object Pattern */

print("\n Null Object Pattern\n***********************************")

let customer1:AbstractCustomer = CustomerFactory.getCustomer(name: "Rob")
let customer2:AbstractCustomer = CustomerFactory.getCustomer(name: "Bob")
let customer3:AbstractCustomer = CustomerFactory.getCustomer(name: "Julie")
let customer4:AbstractCustomer = CustomerFactory.getCustomer(name: "Laura")

print("Customers")

print("\(customer1.name)")
print("\(customer2.name)")
print("\(customer3.name)")
print("\(customer4.name)")

/* Strategy Pattern */

print("\n Strategy Pattern\n***********************************")

var strategyContext : StrategyContext  =  StrategyContext(strategy: OperationAdd())
print("10 + 5 = \(strategyContext.executeStrategy(num1: 10, num2: 5))")

strategyContext   =  StrategyContext(strategy: OperationSubstract())
print("10 - 5 = \(strategyContext.executeStrategy(num1: 10, num2: 5))")

strategyContext   =  StrategyContext(strategy: OperationMultiply())
print("10 * 5 = \(strategyContext.executeStrategy(num1: 10, num2: 5))")

/* Template Pattern */

print("\n Template Pattern\n***********************************")

var game:Game  =  Cricket()
game.play()

game =  Football()
game.play()


/* Visitor Pattern */

print("\n Visitor Pattern\n***********************************")

let computer:ComputerPart =  Computer()
computer.accept(computerPartVisitor: ComputerPartDisplayVisitor());

/* MVC Pattern */

print("\n MVC Pattern\n***********************************")

func retriveStudentFromDatabase()->Student{
    let student:Student  =  Student()
    student.name = "Robert"
    student.rollNo = "10"
    return student
}

let model:Student   = retriveStudentFromDatabase()
let view:StudentView  =  StudentView();

let controller: StudentController  =  StudentController(model: model, view: view);

controller.updateView();

controller.studentName = "John"

controller.updateView();


