////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

class Person{
    var name:String = ""
    var gender:String = ""
    var maritalStatus:String = ""
    
    init(name:String,gender:String,maritalStatus:String){
        self.name = name
        self.gender = gender
        self.maritalStatus = maritalStatus
    }
}

protocol Criteria {
    func meetCriteria(persons:Array<Person>)->Array<Person>
}

class CriteriaMale:Criteria{
    
    func meetCriteria(persons:Array<Person>)->Array<Person>{
        
        var retArray:Array<Person> = Array<Person>()
        for person in persons{
            if person.gender.uppercased() == "MALE"{
                retArray.append(person)
            }
        }
        
        return retArray
    }
}

class CriteriaFemale:Criteria{
    
    func meetCriteria(persons:Array<Person>)->Array<Person>{
        
        var retArray:Array<Person> = Array<Person>()
        for person in persons{
            if person.gender.uppercased() == "FEMALE"{
                retArray.append(person)
            }
        }
        
        return retArray
    }
}

class CriteriaSingle:Criteria{
    
    func meetCriteria(persons:Array<Person>)->Array<Person>{
        
        var retArray:Array<Person> = Array<Person>()
        for person in persons{
            if person.maritalStatus.uppercased() == "SINGLE"{
                retArray.append(person)
            }
        }
        
        return retArray
    }
}

class AndCriteria:Criteria{
    
    private var criteria:Criteria?
    private var otherCriteria:Criteria?
    
    init(criteria:Criteria , otherCriteria:Criteria){
        self.criteria = criteria
        self.otherCriteria = otherCriteria
    }
    
    func meetCriteria(persons:Array<Person>)->Array<Person>{
        
        let firstCriteria:Array<Person> = criteria!.meetCriteria(persons: persons)
        
        return otherCriteria!.meetCriteria(persons: firstCriteria)
    }
}

class OrCriteria:Criteria{
    
    private var criteria:Criteria?
    private var otherCriteria:Criteria?
    
    init(criteria:Criteria , otherCriteria:Criteria){
        self.criteria = criteria
        self.otherCriteria = otherCriteria
    }
    
    func meetCriteria(persons:Array<Person>)->Array<Person>{
        
        var firstCriteriaItems:Array<Person> = criteria!.meetCriteria(persons: persons)
        let otherCriteriaItems:Array<Person> = otherCriteria!.meetCriteria(persons: persons)
        
        for person in otherCriteriaItems {
            if firstCriteriaItems.contains(where: {$0.name == person.name}) == false{
                firstCriteriaItems.append(person)
            }
        }
        return firstCriteriaItems;
    }
}
