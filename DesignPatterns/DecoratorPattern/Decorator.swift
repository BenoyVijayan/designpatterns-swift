////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation


protocol MacBook {
  
    var cost : Double { get }
    var description : String { get }
}

class MacBookAir : MacBook {

    var cost: Double {
        get { return 999 }
    }
    
    var description: String {
        get { return "MacBook Air" }
    }
}

class MacBookPro: MacBook {
    
    var cost: Double {
        get { return 1599 }
    }
    
    var description: String {
        get { return "MacBook Pro" }
    }
}

class MacBookRegular : MacBook {
    
    var cost: Double {
        get { return 1099 }
    }
    
    var description: String {
        get { return "MacBook" }
    }
}

class MacBookDecorator : MacBook{
    
    var cost: Double {
        get { return mbInstance.cost }
    }
    
    var description: String {
        get { return mbInstance.description }
    }
    
    let mbInstance : MacBook
    
    required init(macbook: MacBook) {
        self.mbInstance = macbook
    }
}

final class ProcessorUpgrade : MacBookDecorator {
    
    override var cost: Double {
        get { return mbInstance.cost + 499 }
    }
    
    override var description: String {
        get { return mbInstance.description + ", i7 Processor" }
    }
    
    required init(macbook: MacBook) {
        super.init(macbook: macbook)
    }
    
}

final class SSDUpgrade : MacBookDecorator {
    
    override var cost: Double {
        get { return mbInstance.cost + 299 }
    }
    
    override var description: String {
        get { return mbInstance.description + ", 512gb SSD" }
    }
    
    required init(macbook: MacBook) {
        super.init(macbook: macbook)
    }
}

final class TouchBarUpgrade : MacBookDecorator {
    
    override var cost: Double {
        get { return mbInstance.cost + 199 }
    }
    
    override var description: String {
        get { return mbInstance.description + ", w/ TouchBar" }
    }
    
    required init(macbook: MacBook) {
        super.init(macbook: macbook)
    }
}
