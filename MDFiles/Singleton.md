###Singleton PatternThis pattern involves a single class which is responsible to create an object while making sure that only single object gets created. This class provides a way to access its only object which can be accessed directly without need to instantiate the object of the class.#####Singleton Pattern – Class diagram

![alt text](../Docs/ClassDiagrams/Singleton.png)

#####Singleton Pattern – Swift Code```class Singleton{        static let singleInstance:Singleton = Singleton()        private init(){        print("First instance")    }        func first(){        print("function from single instance - first")    }        func second(){        print("function from single instance - second")    }}/* Singleton Pattern Demo */Singleton.singleInstance.first()Singleton.singleInstance.second()```
#####Singleton Pattern Demo Output```First instancefunction from single instance - firstfunction from single instance - second
```