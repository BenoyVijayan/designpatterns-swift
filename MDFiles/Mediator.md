###Mediator Mediator pattern is used to reduce communication complexity between multiple objects or classes. This pattern provides a mediator class which normally handles all the communications between different classes and supports easy maintenance of the code by loose coupling.
#####Mediator  Pattern – Class diagram

![alt text](../Docs/ClassDiagrams/Mediator.png)

#####Mediator  Pattern – Swift Code```class User {    var name:String = ""    init(name:String){        self.name  = name    }        func sendMessage(message:String){        ChatRoom.showMessage(user: self,message: message);    }}class ChatRoom {        static func showMessage(user:User , message: String){        print("\(Date().description) [\(user.name)] : \(message)")    }}/* Mediator Pattern Demo */let robert:User =  User(name: "Robert")let john:User = User(name: "John")robert.sendMessage(message: "Hi! John!")john.sendMessage(message: "Hello! Robert!")```
#####Mediator  Pattern Demo Output*```2018-02-13 12:23:46 +0000 [Robert] : Hi! John!2018-02-13 12:23:47 +0000 [John] : Hello! Robert!```