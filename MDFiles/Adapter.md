### Adapter Pattern

Adapter pattern works as a bridge between two incompatible interfaces. This type of design pattern comes under structural pattern as this pattern combines the capability of two independent interfaces.

This pattern involves aTsingleyclass which is responsible to join functionalities of independent or incompatible interfaces. A real life example could be a case of card reader which acts as an adapter between memory card and a laptop. You plugin the memory card into card reader and card reader into the laptop so that memory card can be read via laptop.

##### Adapter Pattern – Class diagram

![alt text](../Docs/ClassDiagrams/Adapter.png)

##### Adapter Pattern – Swift Code
```
protocol MediaPlayer {
    func play(audioType:String, fileName:String)
}

protocol AdvancedMediaPlayer {
    func playVlc(fileName:String)
    func playMp4(fileName:String)
}

class VlcPlayer:AdvancedMediaPlayer{
    
    func playVlc(fileName:String){
        print("Playing VLC")
    }
    
    func playMp4(fileName:String){}
}

class Mp4Player:AdvancedMediaPlayer{

    func playMp4(fileName:String){
        print("Playing Mp4")
    }
    
    func playVlc(fileName:String){}
}

class MediaAdapter:MediaPlayer{

    var advancedMediaPlayer:AdvancedMediaPlayer?
    init(audioType:String){
        if audioType == "vlc"{
            advancedMediaPlayer = VlcPlayer()
        }else if audioType == "mp4"{
            advancedMediaPlayer = Mp4Player()
        }
    }
    
    func play(audioType:String , fileName:String){
        if audioType == "vlc"{
            advancedMediaPlayer?.playVlc(fileName: fileName)
        }else if audioType == "mp4"{
            advancedMediaPlayer?.playMp4(fileName: fileName)
        }
    }
}

class AudioPlayer:MediaPlayer{
    func play(audioType:String,fileName:String){
        if audioType == "mp3"{
            print("Playing mp3")
        }else{
            let adapter:MediaAdapter = MediaAdapter(audioType:audioType)
            adapter.play(audioType:audioType,fileName:fileName)
        }
    }

}

/* Adapter Pattern Demo*/

let audioPlayer:AudioPlayer = AudioPlayer()

audioPlayer.play(audioType: "mp3", fileName: "beyond the horizon.mp3")
audioPlayer.play(audioType: "mp4", fileName: "alone.mp4")
audioPlayer.play(audioType: "vlc", fileName: "far far away.vlc")

audioPlayer.play(audioType: "avi", fileName: "mind me.avi")
```

##### *Adapter Pattern Demo Output*
```
Playing mp3
Playing Mp4
Playing VLC
```