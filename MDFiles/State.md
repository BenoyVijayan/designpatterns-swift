###State Pattern In State pattern a class behavior changes based on its state. This type of design pattern comes under behavior pattern.
In State pattern, we create objects which represent various states and a context object whose behavior varies as its state object changes.#####Observer   Pattern – Class diagram

![alt text](../Docs/ClassDiagrams/State.png)

#####State   Pattern – Swift Code```protocol State{    var string:String { get }    func doAction(context:Context)}class StartState:State{        func doAction(context:Context) {        print("Player is in start state")        context.state = self    }        var string:String{        get{ return "Start State" }    }}class StopState: State {        func doAction(context:Context) {        print("Player is in stop state")        context.state = self    }        var string:String{        get{ return "Stop State"; }    }}class Context {    public var state: State? = nil}/* State Pattern Demo*/let context:Context =  Context()let startState:StartState = StartState()startState.doAction(context: context)print(context.state!.string )let stopState:StopState =  StopState()stopState.doAction(context: context)print(context.state!.string )
```
#####State Pattern Demo Output*```Player is in start stateStart StatePlayer is in stop stateStop State```