###Facade PatternFacade pattern hides the complexities of the system and provides an interface to the client using which the client can access the system. This type of design pattern comes under structural pattern as this pattern adds an interface to existing system to hide its complexities.
 This pattern involves a single class which provides simplified methods required by client and delegates calls to methods of existing system classes.#####Facade Pattern – Class diagram

![alt text](../Docs/ClassDiagrams/Facade.png)

#####Facade Pattern – Swift Code```protocol Toy{ func produceToy() }class PlasticToy:Toy{        func produceToy(){        print("Produce toy by plastic moulding")    }}class WoodenToy:Toy{        func produceToy(){        print("Produce toy by wood carving")    }}class ToyMaker{        private var palsticToy:Toy?    private var woodenToy:Toy?        init(){        palsticToy = PlasticToy()        woodenToy = WoodenToy()    }        func makeWoodenToy(){        woodenToy?.produceToy()    }        func makePlasticToy(){        palsticToy?.produceToy()    }}

/* Facade Pattern Demo*/let toyMaker = ToyMaker()toyMaker.makePlasticToy()toyMaker.makeWoodenToy()```
#####Facade Pattern Demo Output*```Produce toy by plastic mouldingProduce toy by wood carving```