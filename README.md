#Software Design patterns – Swift##What is software design pattern?In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem in software design. A design pattern isn't a finished design that can be transformed directly into code. It is a description or template for how to solve a problem that can be used in many different situations.##Advantages of design pattern in software programing **Code readability:** They do help you write more understandable code with better names for what you are trying to accomplish. **Code maintainability:** Allows your code to be maintained easier because it is more understandable. **Communication:** They help you communicate design goals amongst programmers. **Intention:** They show the intent of your code instantly to someone learning the code. **Code re-use:** They help you identify common solutions to common problems. **Less code:** They allow you to write less code because more of your code can derive common functionality from common base classes.
 **Tested and sound solutions:** Most of the design patterns are tested, proven and sound.##Types of design patternAs per the design pattern reference book ‘ Design Patterns - Elements of Reusable Object-Oriented Software’ (GoF) , there are 23 design patterns which can be classified into three categories: **Creational Patterns**  **Structural Patterns** **Behavioral Patterns**

##Creational PatternsThese design patterns provide a way to create objects while hiding the creation logic, rather than instantiating objects directly using new operator (Constructor). This gives program more flexibility in deciding which objects need to be created for a given use case.**[Factory Pattern](MDFiles/Factory.md)**
**[Abstract Factory Pattern](MDFiles/AbstractFactory.md)**
**[Singleton Pattern](MDFiles/Singleton.md)**
**[Builder Pattern](MDFiles/Builder.md)**
**[Prototype Pattern](MDFiles/Prototype.md)**

##Structural PatternsIn Software Engineering, Structural Design Patterns are Design Patterns that ease the design by identifying a simple way to realize relationships between entities.**[Adapter Pattern](MDFiles/Adapter.md)**
**[Bridge Pattern](MDFiles/Bridge.md)**
**[Filter Pattern](MDFiles/Filter.md)**
**[Composite Pattern](MDFiles/Composite.md)**
**[Decorator Pattern](MDFiles/Decorator.md)**
**[Facade Pattern](MDFiles/Facade.md)**
**[Flyweight Pattern](MDFiles/Flyweight.md)**
**[Proxy Pattern](MDFiles/Proxy.md)**

##Behavioral PatternsIn software engineering, behavioral design patterns are design patterns that identify common communication patterns between objects and realize these patterns. By doing so, these patterns increase flexibility in carrying out this communication.**[Chain of responsibility](MDFiles/ChainOfResponsibility.md)****[Command](MDFiles/Command.md)**

**[Interpreter](MDFiles/Interpreter.md)**
**[Iterator](MDFiles/Iterator.md)**

**[Mediator](MDFiles/Mediator.md)**
**[Memento](MDFiles/Memento.md)**
**[Null Object](MDFiles/NullObject.md)**
**[Observer](MDFiles/Observer.md)**
**[State](MDFiles/State.md)**
**[Strategy](MDFiles/Strategy.md)**
**[Template](MDFiles/Template.md)**
**[Visitor](MDFiles/Visitor.md)**